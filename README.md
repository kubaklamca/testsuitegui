# TestSuitesGUI

TestSuitesGUI is cross platform desktop application written in wxPython and Python 3.7 to provide readable and light graphic user interface for randomness test suites - NIST, Testu01 and Dieharder


## Planned functionalities
* Automatic installation of Test suites
* Main window when you can run the suites on given binary file
* Config windows when you can change parameters of tools
* Non complicated structure of results of tests which is a problem right now (You have to know documentation to know where given tests are saved)