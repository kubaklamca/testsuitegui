import distutils.util
import wx
from configwindow import ConfigWindow
from config import config

config_window = None

class MainWindow(wx.Frame):

    bin_file = None

    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, wx.ID_ANY, title=title, style=wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL)

        # Controls
        self.panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize)
        panel_sizer = wx.BoxSizer(wx.VERTICAL)
        panel_sizer.Add(self.panel, 1, wx.EXPAND|wx.ALL, 0)

        file_menu= wx.Menu() 
        about_menu = wx.Menu()

        
        load_button = wx.Button(self.panel, wx.ID_ANY, 'Load .bin file')

        self.nist_box = wx.CheckBox(self.panel, label='NIST')
        self.testu_box = wx.CheckBox(self.panel, label='Testu01')
        self.dieharder_box = wx.CheckBox(self.panel, label='Dieharder')


        # Sizers
        vertical_sizer = wx.BoxSizer(wx.VERTICAL)
    
        load_sizer = wx.BoxSizer(wx.HORIZONTAL)
        test_sizer = wx.StaticBoxSizer(wx.StaticBox(self.panel, label='Choose test suites'),orient=wx.HORIZONTAL)
        bottom_sizer = wx.BoxSizer(wx.HORIZONTAL)
        empty_sizer = wx.BoxSizer(wx.HORIZONTAL)


        load_sizer.Add(load_button, 1, wx.LEFT|wx.RIGHT, 30)

        test_sizer.Add(self.nist_box, 1, wx.EXPAND|wx.RIGHT, 10)
        test_sizer.Add(self.testu_box, 1, wx.EXPAND|wx.RIGHT, 10)
        test_sizer.Add(self.dieharder_box, 1, wx.EXPAND|wx.RIGHT, 10)

        close_button = wx.Button(self.panel, wx.ID_ANY, label='Exit')

        bottom_sizer.Add(
            close_button,
            0,
            wx.RIGHT,
            5
        )

        config_button = wx.Button(self.panel, wx.ID_ANY, label='Open configuration')

        bottom_sizer.Add(
          config_button,
            0,
        )

        bottom_sizer.Add(
            wx.Button(self.panel, wx.ID_ANY, label='Start tests'),
            0,
            wx.LEFT,
            5
        )
        
        # Dummy element
        empty_sizer.Add(
            wx.StaticText(self.panel, label=''),
            1
        )
        vertical_sizer.Add(load_sizer, 0, wx.ALL|wx.EXPAND, 15)
        vertical_sizer.Add(test_sizer, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5)
        vertical_sizer.Add(empty_sizer, 1, wx.ALL| wx.EXPAND, 20)
        vertical_sizer.Add(bottom_sizer, 0, wx.ALL|wx.ALIGN_RIGHT, 5)

        

        # Events
        config_button.Bind(wx.EVT_BUTTON, self._open_config_window)
        load_button.Bind(wx.EVT_BUTTON, self._load_binary)
        close_button.Bind(wx.EVT_BUTTON, self._close)
        self.panel.SetSizer(vertical_sizer)
        self.SetSizer(panel_sizer)
        self._load_data()
        self.Layout()
        self.Show(True)
    

    def _load_data(self):
        self.nist_box.SetValue(bool(distutils.util.strtobool(config['testsuites']['NIST'])))
        self.testu_box.SetValue(bool(distutils.util.strtobool(config['testsuites']['testu01'])))
        self.dieharder_box.SetValue(bool(distutils.util.strtobool(config['testsuites']['dieharder'])))

    def _save_data(self):
        config['testsuites']['NIST'] = self.nist_box.GetValue()
        config['testsuites']['testu01'] = self.testu_box.GetValue()
        config['testsuites']['dieharder'] = self.dieharder_box.GetValue()
        config.write()

    def Close(self):
        self._save_data()
        super(MainWindow, self).Close()
    
    
    def _close(self, event):
        self.Close()


    def _open_config_window(self, event):
        '''Only one config window can be opened at once'''
        global config_window
        if not config_window:
            config_window = ConfigWindow(self, 'Configuration')
            config_window.Show()


    def _load_binary(self, event):
        '''Method that opens file explorer to load binary stream file'''
        with wx.FileDialog(self, "Load .bin file", wildcard="Binary files (*.bin)|*.bin",
                          style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as file_dialog:
            
            if file_dialog.ShowModal() == wx.ID_CANCEL:
                return
            
            path = file_dialog.GetPath()

            try:
                with open(path, 'r') as file:
                    bin_file = file.read()
            except IOError:
                wx.LogError('Cannot open file "%s" ' % newfile)


if __name__ == '__main__':
    app = wx.App(False)
    frame = MainWindow(None, 'Test suites GUI')
    app.MainLoop()