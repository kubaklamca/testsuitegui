import wx
import wx.lib.scrolledpanel as scrolled
from config import config


class TestuTab(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)


        vertical_sizer = wx.BoxSizer(wx.VERTICAL)
 
        self.battery_radio = wx.RadioBox(
            self, 
            label='Choose test battery',
            choices = ['Rabbit', 'Alphabit', 'BlockAlphabit']
            )
        


        empty_sizer = wx.BoxSizer(wx.HORIZONTAL)
        empty_sizer.Add(
            wx.StaticText(self, label=''),
            1
        )

        vertical_sizer.Add(self.battery_radio, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 10)
        vertical_sizer.Add(empty_sizer, 1, wx.ALL|wx.EXPAND, 5)
        # vertical_sizer.Add(bottom_sizer, 0, wx.ALL|wx.ALIGN_RIGHT, 5)


        self.SetSizer(vertical_sizer)

class NISTTab(scrolled.ScrolledPanel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        vertical_sizer = wx.BoxSizer(wx.VERTICAL)
        available_tests = [
            'Frequency',
            'Block Frequency',
            'Cumulative Sums',
            'Runs',
            'Longest Run of Ones',
            'Rank',
            'Discrete Fourier Transform',
            'Nonperiodic Template Matchings',
            'Overlapping Template Matchings',
            'Universal Statistical',
            'Approximate Entropy',
            'Random Excursions',
            'Random Excursions Variant',
            'Serial',
            'Linear Complexity'
        ]
        '''
        Parameter Adjustments
        1. Block Frequency Test - block length (M) - 128
        2. NonOverlapping Template Test - block length(m) - 1
        3. Overlapping Template Test - block length(m) - 9
        4. Approximate Entropy Test - block length(m) - 10
        5. Serial Test - block length(m)- 16
        6. Linear Complexity Test - block length (M) - 500
        '''


        checklistbox_sizer = wx.StaticBoxSizer(
            wx.StaticBox(self, label='Choose tests to run'),
            orient=wx.HORIZONTAL
        )


        self.test_checklistbox = wx.CheckListBox(
            self,
            choices=available_tests,
            style=0,
            name='Select tests'
                )
        
        checklistbox_sizer.Add(self.test_checklistbox, 1, wx.EXPAND|wx.ALL, 5)
        
        parameter_sizer = wx.StaticBoxSizer(
            wx.StaticBox(self, label='Parameter adjustments'),
            orient=wx.VERTICAL
        )

        self.block_frequency_input = wx.SpinCtrl(self)
        self.nonoverlapping_template_input = wx.SpinCtrl(self)
        self.overlapping_template_input = wx.SpinCtrl(self)
        self.approximate_entropy_input = wx.SpinCtrl(self)
        self.serial_input = wx.SpinCtrl(self)
        self.linear_complexity_input = wx.SpinCtrl(self)

        self.spin_ctrls = [
            self.block_frequency_input,
            self.nonoverlapping_template_input,
            self.overlapping_template_input,
            self.approximate_entropy_input,
            self.serial_input,
            self.linear_complexity_input
        ]

        static_texts = [
            'Block Frequency Test - block length (M):',
            'NonOverlapping Template Test - block length(m):',
            'Overlapping Template Test - block length(m):',
            'Approximate Entropy Test - block length(m):',
            'Serial Test - block length(m):',
            'Linear Complexity Test - block length (M):'
        ]

        for row in [self._generate_parameter_row(*_) for _ in zip(static_texts, self.spin_ctrls)]:
            parameter_sizer.Add(row, 1, wx.ALL|wx.EXPAND, 5)
        
        vertical_sizer.Add(checklistbox_sizer, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 10)
        vertical_sizer.Add(parameter_sizer, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.SetSizer(vertical_sizer)
        self.Layout()
        self.SetupScrolling()

    
    def _generate_parameter_row(self, static_text, spin_ctrl):
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(
            wx.StaticText(self, label=static_text),
            1,
            wx.TOP,
            3
        )

        sizer.Add(spin_ctrl, 0, wx.LEFT, 5)
        return sizer

class DieharderTab(wx.Panel):
    pass


class ConfigWindow(wx.Frame):

    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, wx.ID_ANY, title=title, style=wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL, size=(400,600))
        self.panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize)
        bottom_sizer = wx.BoxSizer(wx.HORIZONTAL)
        
        close_button = wx.Button(self, wx.ID_ANY, 'Close')
        save_button = wx.Button(self, wx.ID_ANY, 'Save')

        # Bottom buttons should be common for all tabs...
        bottom_sizer.Add(close_button, 0, wx.LEFT, 5)
        bottom_sizer.Add(save_button, 0, wx.LEFT, 5)
        

        
        panel_sizer = wx.BoxSizer(wx.VERTICAL)
        panel_sizer.Add(self.panel, 1, wx.EXPAND|wx.ALL, 0)
        
        notebook = wx.Notebook(self.panel)
        
        self.testu_tab = TestuTab(notebook)
        self.nist_tab = NISTTab(notebook)
        self.dieharder_tab = wx.Panel(notebook)

        notebook.AddPage(self.testu_tab, 'Testu01')
        notebook.AddPage(self.nist_tab, 'NIST')
        notebook.AddPage(self.dieharder_tab, 'Dieharder')

        notebook_sizer = wx.BoxSizer()
        notebook_sizer.Add(notebook, 1, wx.EXPAND)
        
        # panel_sizer.Add(wx.ScrollBar())
        panel_sizer.Add(bottom_sizer, 0, wx.LEFT, 5)
        self.panel.SetSizer(notebook_sizer)
        self.SetSizer(panel_sizer)
        self._load_data()
        self.Layout()
        close_button.Bind(wx.EVT_BUTTON, self._close)
        save_button.Bind(wx.EVT_BUTTON, self._save_config)

    def _close(self, event):
        self.Close()

    def _save_config(self, event):
        config['NIST']['parameters']['block_frequency'] = self.nist_tab.block_frequency_input.GetValue()
        config['NIST']['parameters']['nonoverlapping_template'] = self.nist_tab.nonoverlapping_template_input.GetValue()
        config['NIST']['parameters']['overlapping_template'] = self.nist_tab.overlapping_template_input.GetValue()
        config['NIST']['parameters']['approximate_entropy'] = self.nist_tab.approximate_entropy_input.GetValue()
        config['NIST']['parameters']['serial'] = self.nist_tab.serial_input.GetValue()
        config['NIST']['parameters']['linear_complexity'] = self.nist_tab.linear_complexity_input.GetValue()
        config['NIST']['checked_tests'] = self.nist_tab.test_checklistbox.GetCheckedItems()

        config['testu01']['battery'] = self.testu_tab.battery_radio.GetStringSelection()
        config.write()


    def _load_data(self):
        self.nist_tab.block_frequency_input.SetValue(config['NIST']['parameters']['block_frequency']) 
        self.nist_tab.nonoverlapping_template_input.SetValue(config['NIST']['parameters']['nonoverlapping_template'])
        self.nist_tab.overlapping_template_input.SetValue(config['NIST']['parameters']['overlapping_template'])
        self.nist_tab.approximate_entropy_input.SetValue(config['NIST']['parameters']['approximate_entropy'])
        self.nist_tab.serial_input.SetValue(config['NIST']['parameters']['serial'])
        self.nist_tab.linear_complexity_input.SetValue(config['NIST']['parameters']['linear_complexity'])
        self.nist_tab.test_checklistbox.SetCheckedItems([int(num) for num in config['NIST']['checked_tests']])

        self.testu_tab.battery_radio.SetStringSelection(config['testu01']['battery'])